package com.softclub.locationbot.domain.dto;

import lombok.Data;

import java.util.List;

@Data
public class Day {
    private List<Activity> activities;
    private int daynumber;
}