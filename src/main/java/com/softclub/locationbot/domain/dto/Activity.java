package com.softclub.locationbot.domain.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.softclub.locationbot.utils.UnixTimestampDeserializer;
import lombok.Data;

import java.util.Date;

@Data
public class Activity {

    @JsonDeserialize(using = UnixTimestampDeserializer.class)
    private Date dateTime;
    private String direction;
    private String locationId;
}