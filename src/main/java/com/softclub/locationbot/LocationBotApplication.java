package com.softclub.locationbot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class LocationBotApplication {

    public static void main(String[] args) {
        SpringApplication.run(LocationBotApplication.class, args);
    }

}
