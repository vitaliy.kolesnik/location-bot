package com.softclub.locationbot.service.impl;

import com.softclub.locationbot.domain.dto.RateDto;
import com.softclub.locationbot.service.RateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
public class RateServiceImpl implements RateService {

    @Value(value = "${bank.api.rate}")
    private String url;

    private final RestTemplate restTemplate;

    public RateServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public List<RateDto> getRates(String city) {
        URI verifyUri = URI.create(String.format(url, city));

        return Arrays.asList(Objects.requireNonNull(restTemplate.getForObject(verifyUri, RateDto[].class)));
    }

}
