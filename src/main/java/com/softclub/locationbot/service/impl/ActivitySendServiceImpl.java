package com.softclub.locationbot.service.impl;

import com.softclub.locationbot.domain.dto.Day;
import com.softclub.locationbot.service.ActivityService;
import com.softclub.locationbot.service.SendService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Map;
import java.util.Random;

/**
 * @author Timofei Lyahor (t.lyahor@softclub.by)
 */
@Service
@Slf4j
public class ActivitySendServiceImpl implements SendService {

//    @Autowired
//    private KafkaTemplate<Long, Day> kafkaTemplate;
//
//    @Autowired
//    private ActivityService activityService;

    @Override
//    @Scheduled(cron = "0 0/1 * 1/1 * ?")
    public void send() {
//        var date = LocalDate.now();
//
//        final Random random = new Random();
//        var day = random.nextInt(date.getDayOfMonth());
//        log.info("Get info " + day);
//        var activity = activityService.getActivity(day);
//        if (activity != null) {
//            kafkaTemplate.send("softclub", activity);
//        }
    }

}
