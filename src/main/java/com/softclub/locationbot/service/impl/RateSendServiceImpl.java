package com.softclub.locationbot.service.impl;

import com.softclub.locationbot.domain.dto.RateDto;
import com.softclub.locationbot.service.RateService;
import com.softclub.locationbot.service.SendService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class RateSendServiceImpl implements SendService {

    private final KafkaTemplate<Long, RateDto> kafkaRateTemplate;

    private final RateService rateService;

    public RateSendServiceImpl(KafkaTemplate<Long, RateDto> kafkaRateTemplate,
                               RateService rateService) {
        this.kafkaRateTemplate = kafkaRateTemplate;
        this.rateService = rateService;
    }

    @Override
    @Scheduled(cron = "0 0/1 * 1/1 * ?")
    public void send() {
        var dto = new RateDto();

        List<RateDto> rates = rateService.getRates("Минск");
        if (rates != null) {
            Optional<RateDto> optDto = rates.stream()
                    .findFirst();
            if (optDto.isPresent()) {
                dto = optDto.get();
                log.info(dto.toString());
                kafkaRateTemplate.send("softclub", dto);
            }
        }

    }
}
