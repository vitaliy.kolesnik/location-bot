package com.softclub.locationbot.service.impl;

import com.softclub.locationbot.domain.dto.Day;
import com.softclub.locationbot.service.ActivityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.*;

@Service
@Slf4j
public class ActivityServiceImpl implements ActivityService {

    @Autowired
    private RestTemplate restTemplate;

    private Map<String, String> placeMap = Map.ofEntries(
            Map.entry("13", "Независимости, 168/1"),
            Map.entry("3", "Ложинская 4"),
            Map.entry("10", "Независимости, 170"),
            Map.entry("11", "Независимости, 170"),
            Map.entry("12", "VIP зона"),
            Map.entry("5", "Независимости, 172"),
            Map.entry("6", "Независимости, 181")
    );

    @Value(value = "${location.url}")
    private String url;

    @Value(value = "${employee.id}")
    private int empId;

    @Override
    public Day getActivity(int day) {

        var now = LocalDate.now();
        int month = now.getMonth().getValue();
        int year = now.getYear();

        ResponseEntity<Day[]> response
                = restTemplate.getForEntity(String.format(url, empId, month, year), Day[].class);
        Day[] days = response.getBody();

        if (days != null && days.length > 0) {
            Optional<Day> optDay = Arrays.stream(days)
                    .filter(day1 -> day1.getDaynumber() == day)
                    .findFirst();
            //                List<String> activities =
            //                        dayActivity.getActivities().stream()
            //                                .map(a -> "Дата: " + a.getDateTime() + " " +
            //                                        (a.getDirection().toLowerCase().equals("in") ? " вошел в " : " вышел из ") + placeMap.get(a.getLocationId()) + "\n")
            //                                .collect(Collectors.toList());
            //                String result = activities.toString();
            //                log.info(result);
            //                return result;
            return optDay.orElse(null);
        } else {
            return null;
        }
    }

}
