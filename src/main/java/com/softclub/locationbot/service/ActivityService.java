package com.softclub.locationbot.service;

import com.softclub.locationbot.domain.dto.Day;

public interface ActivityService {
    Day getActivity(int day);
}
