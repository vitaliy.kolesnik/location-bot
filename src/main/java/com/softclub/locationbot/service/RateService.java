package com.softclub.locationbot.service;

import com.softclub.locationbot.domain.dto.RateDto;

import java.util.List;

public interface RateService {
    List<RateDto> getRates(String city);
}
