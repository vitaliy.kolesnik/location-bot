package com.softclub.locationbot;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.Random;

@SpringBootTest
class LocationBotApplicationTests {

    @Test
    void contextLoads() {
        var date = LocalDate.now();
        final Random random = new Random();
        var day = random.nextInt(date.getDayOfMonth());
        System.out.println("Get info " + day);
    }

}
